﻿using System;

namespace Exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();

            Console.WriteLine("Ex 21 - Part A");
            
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine($"{i+1}");
            }

            Console.WriteLine("\n");
            Console.WriteLine("Ex 21 - Part B");

            int j = 1;
            while (j < 6)
            {
                Console.WriteLine($"{j}");
                j++;
            }

            Console.WriteLine("\n");
            Console.WriteLine("Ex 21 - Part C - for loop");

            for (int i = 0; i <= 20; i++)
            {
                if (i % 2 == 0)
                {
                    Console.WriteLine($"{i}");
                }
            }

            Console.WriteLine("\n");
            Console.WriteLine("Ex 21 - Part C - while loop");

            int k = 0;
            while (k < 21)
            {
                if (k % 2 == 0)
                {
                    Console.WriteLine($"{k}");
                }
                k++;
            }

            Console.WriteLine("\n");
            Console.WriteLine("Ex 21 - Part D");
            
            int index = 50;
            int counter = 10;
            // I do not understand how we could need an if statement?
            // I've read through the question a bunch of times but I'm unsure on exactly what we're supposed to do.
            do
            {
                if (index > counter)
                {
                    Console.WriteLine($"Index = {index} | Counter = {counter}");
                }
                index++;
            } while (index < counter);

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
